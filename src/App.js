import React from 'react';
import SurveyCreator from "./SurveyCreator";
import './App.css';

function App() {
  return (
    <div className="App">
      <h2>Survey Creator - create a new survey</h2>
        <SurveyCreator />
    </div>
  );
}

export default App;
